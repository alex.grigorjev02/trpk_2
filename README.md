# TRPK_2



## Постановка задачи
Разработать чат-бота в Telegram который позволяет по базе номеров в таблице находить пользователей и добавлять их в телеграм канал.

## Что необходимо сделать?

 Получать номера телефонов из табличного документа и сохранять id пользователей, соответствующих номерам в документе. Добавлять контакты по id в канал. 
 Необходимо предусмотреть возможность работы бота с локальными файлами на компьютере пользователя, а так же с фалами доступными по ссылке через сервисы Google Docs и Яндекс.Документы. Пользователь должен иметь возможность регулировать скорость добавления контактов в канал, а так же устанавливать суточный лимит. Лимит добавлений не должен превышать 200 в сутки.

## Требования к проекту и доступы

 Перед началом работы подготовить блок-схему работы чат-бота, отобразить зависимости, показать логику работы. Обосновать выбор стека разработки. Объяснить причину его выбора сравнить с другими возможными реализациями. Обосновать эффективность выбранного стека.  
