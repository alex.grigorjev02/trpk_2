import openpyxl
from openpyxl import load_workbook

d = {} #Создаём словарь

wb = load_workbook('./example.xlsx') #Загружаем XLSX
sheet = wb['test'] #Переходим на нужный лист

for i in range(1, 200): #Формируем словарь
    if (sheet.cell(row=i, column=1).value != None and sheet.cell(row=i, column=1).value != None):
        d.setdefault(i, [sheet.cell(row=i, column=1).value, sheet.cell(row=i, column=2).value])

print(d)
    
#Key: [Item1,  Item2]
#   ID: [Имя,    Номер]
     
