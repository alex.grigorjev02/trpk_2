import telebot
import os
import asyncio
from telebot.async_telebot import AsyncTeleBot
import config

bot = AsyncTeleBot(config.token)

async def make_markup_to_stop():
    markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
    buttonA = telebot.types.KeyboardButton('Остановить добавление')
    markup.row(buttonA)
    return markup

async def make_markup_upload():
    markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
    buttonB = telebot.types.KeyboardButton('Загрузить документ с номерами')
    setting = telebot.types.KeyboardButton('Настройки')
    back = telebot.types.KeyboardButton('Вернуться в главное меню')
    markup.row(buttonB,setting,back)

    return markup


@bot.message_handler(commands=['start'])
async def start_message(message):
    await bot.send_message(message.chat.id, 'Бот создан для добавления пользователей в канал из файла (недоделанный)')

    markup = await make_markup_upload()
    await bot.send_message(message.chat.id, 'Выберите необходимый пункт',
                     reply_markup=markup)


@bot.message_handler(content_types=['document'])
async def get_file(message):

    files_dir = f"user/{message.chat.id}"
    file_name = message.document.file_name
    file_extension = file_name.split('.')[-1]
    if file_extension != 'xlsx':
        markup = make_markup_upload()
        await bot.send_message(message.chat.id, 'Бот умеет обрабатывать только xlsx файлы', reply_markup=markup)
    else:
        file_info = await bot.get_file(message.document.file_id)
        downloaded_file = await bot.download_file(file_info.file_path)
        if os.path.exists(files_dir):
            counter = len(os.listdir(files_dir))
            with open(f"{files_dir}/file_{counter}.xlsx", 'wb') as new_file:
                new_file.write(downloaded_file)
        else:
            os.mkdir(files_dir)
            with open(f"{files_dir}/file_0.xlsx", 'wb') as new_file:
                new_file.write(downloaded_file)
        markup = await make_markup_to_stop()
        await bot.send_message(message.chat.id, 'Файл успешно загружен. Сейчас начнется добавление в канал', reply_markup=markup)


@bot.message_handler(content_types=['text']) 
async def function(message):
    if message.text == 'Остановить добавление':
        await bot.send_message(message.chat.id, 'Добавление остановлено')
    elif message.text == 'Загрузить документ с номерами':
        await bot.send_message(message.chat.id, 'Вставьте документ с расширением xlsx')
    elif message.text == 'Настройки':
        markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
        speed = telebot.types.KeyboardButton('Установить скорость добавления')
        link = telebot.types.KeyboardButton('Установить ссылку на канал')
        limit = telebot.types.KeyboardButton('Установить лимит')
        back = telebot.types.KeyboardButton('Вернуться в главное меню')
        markup.add(speed,link,limit,back)
        await bot.send_message(message.chat.id, 'Какие настройки?', reply_markup=markup)
    elif message.text == 'Вернуться в главное меню':
        markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
        buttonB = telebot.types.KeyboardButton('Загрузить документ с номерами')
        setting = telebot.types.KeyboardButton('Настройки')
        back = telebot.types.KeyboardButton('Вернуться в главное меню')
        markup.row(buttonB,setting,back)
        await bot.send_message(message.chat.id, 'Вы вернулись в главное меню', reply_markup=markup)
    elif message.text == 'Установить скорость добавления':
        await bot.send_message(message.chat.id, 'Укажите интервал между добавлениями (в минутах)')
    elif message.text == 'Установить ссылку на канал':
        await bot.send_message(message.chat.id, 'Укажите id канала, куда добавлять')
    elif message.text == 'Установить лимит':
        await bot.send_message(message.chat.id, 'Укажите лимит добавления (за сутки, не более 200)')
asyncio.run(bot.polling())
